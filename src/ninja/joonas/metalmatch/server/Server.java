package ninja.joonas.metalmatch.server;

import ninja.joonas.metalmatch.server.util.ConnectionHandler;
import ninja.joonas.metalmatch.server.util.Constant;
import ninja.joonas.metalmatch.server.util.FileUtil;
import ninja.joonas.metalmatch.server.util.ServerRuntime;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The main class which starts the server.
 * Created by joonas on 20.8.2015.
 */
public class Server {
    /**
     * The main method.
     * @param args
     */
    public static void main(String[]args){
        init();
    }

    /**
     * Pool for the various connection threads.
     */
    private static ExecutorService threadExecutor;
    private static List<ConnectionHandler> connections;
    private static ServerSocket serverSocket;
    private static ServerListener listener;

    public static void init(){
        try{
            ServerRuntime.initRuntimeData();
            connections = new ArrayList<ConnectionHandler>();
            serverSocket = new ServerSocket(Integer.valueOf(ServerRuntime.getServerConfs().get(Constant.CONF_PORT)));
            threadExecutor = Executors.newFixedThreadPool(Integer.valueOf(ServerRuntime.getServerConfs().get(Constant.CONF_MAX_THREADS)));
            listener = new ServerListener();
        }catch(IOException e){
            e.printStackTrace();
            throw new RuntimeException("Error starting server");
        }
    }

    public static void shutdown(int code){
        try{
            listener.stopListening();
            if(ServerRuntime.isLoggingEnabled()){
                FileUtil.Logger.getLogger().close();
            }
            serverSocket.close();
            System.exit(code);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * The listener thread which listens for new connections.
     */
    private static class ServerListener extends Thread{
        private boolean listening = false;

        public ServerListener(){
            listening = true;
            FileUtil.Logger.logInfo(Constant.INFO_START);
            start();
        }

        public void stopListening(){
            listening = false;
        }

        @Override
        public void run(){
            while(listening){
                try{
                    Socket socket = serverSocket.accept();
                    ConnectionHandler handler = new ConnectionHandler(socket);
                    connections.add(handler);
                    threadExecutor.execute(handler);
                }catch(IOException e){
                    FileUtil.Logger.logErrorStream(e);
                    shutdown(10);
                }
            }
        }
    }
}
