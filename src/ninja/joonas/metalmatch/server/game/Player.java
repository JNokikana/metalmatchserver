package ninja.joonas.metalmatch.server.game;

import ninja.joonas.metalmatch.server.util.Constant;

/**
 * Created by joonas on 20.8.2015.
 */
public class Player {
    private String playerId;
    private String name;
    /**
     * Has the players client loaded the assets so the player can now enter the game?
     * The server does not accept any move or shoot requests from the player before
     * it receives the load ready request.
     */
    private boolean ready;
    private float x;
    private float y;
    private int score;
    private float hp;
    /**
     * Cooldown for the players weapon. The player cannot shoot while cooldown is ongoing.
     */
    private int cooldown;

    /**
     * Game has some powerups. Speed of the player.
     */
    private int playerSpeed;

    /**
     * The current armour value of the player.
     */
    private float armour;

    /**
     * How much is the players current cooldown. ie. what is the
     * players current firing rate.
     */
    private float cooldownValue;

    /**
     * How much damage the player currently does.
     */
    private float damage;

    public Player(String name, String id){
        this.name = name;
        this.damage = Constant.START_DAMAGE;
        this.hp = Constant.HP_MAX;
        this.armour = Constant.START_ARMOUR;
        this.cooldownValue = Constant.START_COOLDOWN;
        this.playerId = id;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public void setArmour(float armour) {
        this.armour = armour;
    }

    public float getCooldownValue() {
        return cooldownValue;
    }

    public void setCooldownValue(float cooldownValue) {
        this.cooldownValue = cooldownValue;
    }

    public float getArmour() {
        return armour;
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getPlayerSpeed() {
        return playerSpeed;
    }

    public void setPlayerSpeed(int playerSpeed) {
        this.playerSpeed = playerSpeed;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String privateId) {
        this.playerId = privateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
