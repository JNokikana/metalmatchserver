package ninja.joonas.metalmatch.server.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * A class which handles connections from one connected client.
 * Created by joonas on 20.8.2015.
 */
public class ConnectionHandler extends Thread{
    private boolean listening;
    private BufferedReader inputStream;
    private Socket socket;

    public ConnectionHandler(Socket socket){
        try{
            System.out.println("Connection!!!");
            this.socket = socket;
            listening = true;
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }catch(Exception e){
            FileUtil.Logger.logErrorStream(e);
            disconnect();
        }
    }

    public void disconnect(){
        try{
            listening = false;
            socket.close();
        }catch(IOException e){
            FileUtil.Logger.logErrorStream(e);
        }
    }

    @Override
    public void run(){

        while(listening){

        }
    }
}
