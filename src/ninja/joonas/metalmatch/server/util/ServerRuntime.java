package ninja.joonas.metalmatch.server.util;

import ninja.joonas.metalmatch.server.game.GameState;
import java.util.Map;

/**
 * This class contains static runtime data that the server uses. Like for instance configurations
 * that have been loaded on initialization, player data and GameState information.
 * Created by joonas on 20.8.2015.
 */
public class ServerRuntime {
    private static Map<String, String> serverConfs;
    private static GameState gameState;
    private static boolean loggingEnabled = false;

    public static void initRuntimeData(){
        gameState = new GameState();
        serverConfs = FileUtil.initConfs();
        loggingEnabled = Boolean.valueOf(serverConfs.get(Constant.CONF_LOGGING));

        if(loggingEnabled){
            FileUtil.Logger.initLogging();
        }
    }

    public static Map<String, String> getServerConfs() {
        return serverConfs;
    }

    public static boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    public static void setLoggingEnabled(boolean loggingEnabled) {
        ServerRuntime.loggingEnabled = loggingEnabled;
    }

    public static void setServerConfs(Map<String, String> s) {
        serverConfs = s;
    }

    public static GameState getGameState(){
        return gameState;
    }
}
