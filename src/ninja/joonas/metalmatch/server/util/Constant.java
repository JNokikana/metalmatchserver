package ninja.joonas.metalmatch.server.util;

/**
 * Created by joonas on 20.8.2015.
 */
public abstract class Constant {
    public static final String FILENAME_LOG = "server.log";
    public static final String FILENAME_CONF = "server.conf";

    public static final String CONF_PORT = "port";
    public static final String CONF_MAX_PLAYERS = "maxPlayers";
    public static final String CONF_PASSWORD = "password";
    public static final String CONF_MAPCYCLE = "mapCycle";
    public static final String CONF_MESSAGE = "message";
    public static final String CONF_NAME = "serverName";
    public static final String CONF_MAX_THREADS = "maxThreads";
    public static final String CONF_LOGGING = "logging";
    public static final String CONF_DURATION = "roundDuration";
    public static final String CONF_MAP_DURATION = "mapDuration";

    public static final String CONF_SYNTAX_COMMENT = "#";
    public static final String CONF_SYNTAX_OPERATOR = "=";
    public static final int CONF_SYNTAX_SIZE = 2;

    public static final String INFO_START = "Server started.";

    public static final float HP_MAX = 100;
    public static final float START_COOLDOWN = 3;
    public static final float START_ARMOUR = 0;
    public static final float START_DAMAGE = 2;

}
