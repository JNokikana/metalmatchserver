package ninja.joonas.metalmatch.server.util;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by joonas on 20.8.2015.
 */
public class FileUtil {
    private static PrintWriter logWriter;
    private static BufferedReader fileReader;

    /**
     * Reads server confs from the conf file.
     * @return
     */
    public static Map<String, String> initConfs(){
        File file = new File(Constant.FILENAME_CONF);
        Map<String, String> confs = new TreeMap<String, String>();
        try{
            if(file.isFile()){
                fileReader = new BufferedReader(new FileReader(file));
                String readLine;
                String [] confArray;

                while((readLine = fileReader.readLine()) != null){
                    if(!readLine.startsWith(Constant.CONF_SYNTAX_COMMENT) && !readLine.isEmpty()){
                        confArray = readLine.split(Constant.CONF_SYNTAX_OPERATOR);
                        if(confArray.length > Constant.CONF_SYNTAX_SIZE){
                            throw new RuntimeException("Too many parameters on row.");
                        }
                        /* After splitting the conf we then put the conf name and value to
                        the map.
                         */
                        confs.put(confArray[0], confArray[1]);
                    }
                }
                fileReader.close();
            }
            else{
                throw new RuntimeException("server.conf is not a file!");
            }
        }catch(Exception e){
            System.err.println("Error reading server.conf.");
            e.printStackTrace();
            try{
                if(fileReader != null){
                    fileReader.close();
                }
            }catch(IOException r){
                r.printStackTrace();
            }
            System.exit(10);
        }
        return confs;
    }

    public static class Logger{
        public static void initLogging(){
            try{
                File file = new File(Constant.FILENAME_LOG);
                /* We delete the log file if it already exists. */
                file.delete();
                logWriter = new PrintWriter(new BufferedWriter(new FileWriter(file)), true);
            }catch(IOException e){
                System.err.println("Error initializing logging.");
                e.printStackTrace();
                if(logWriter != null){
                    logWriter.close();
                }
                System.exit(10);
            }
        }

        public static PrintWriter getLogger(){
            return logWriter;
        }

        public static void logInfo(String text){
            System.out.println(text);
            if(ServerRuntime.isLoggingEnabled()){
                logWriter.println(text);
            }
        }

        public static void logError(String text){
            System.err.println(text);
            if(ServerRuntime.isLoggingEnabled()){
                logWriter.println(text);
            }
        }

        public static void logErrorStream(Exception e){
            e.printStackTrace();
            if(ServerRuntime.isLoggingEnabled()){
                e.printStackTrace(logWriter);
            }
        }
    }
}
